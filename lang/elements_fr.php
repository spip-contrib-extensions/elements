<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'cfg_label_ajouter_element' => 'Ajouter un élément',
	'cfg_label_choisir_elements' => 'Éléments utilisables',
	'cfg_label_choisir_objets' => 'Objets utilisables',
	'choisir_et_configurer_les_elements' => 'Choisir et configurer les éléments',
	'configurer_element' => 'Configurer l\'élément',

	// E
	'element_aucun' => 'Aucun élément',
	'element_un' => '1 élément',
	'elements_nb' => '@nb@ éléments',
	'elements_titre' => 'Éléments',
	'element_texte_titre' => 'Texte',
	'element_texte_description' => 'Texte libre',
	'elements_selectionnes_aucun' => 'Aucun élément à afficher…',
	'elements_selectionnes_pour' => 'Éléments séléctionnés dans le bloc @bloc@',
	'elements_choisis' => 'Éléments actifs',
	'element_type_introuvable' => 'Type d\'élément introuvable',
	'elements_sauvegardes' => 'Les éléments ont été enregistrés',

	// F
	'fermer_les_elements' => 'Fermer',

	// M
	'modifier_les_elements' => 'Modifier',
	'message_element_plus' => 'Un élément a été ajouté.',
	'message_element_moins' => 'Un élément a été enlevé',
	'message_element_deplace' => 'Un élément a été déplacé',
	'message_element_enregistrer' => 'Pensez à enregistrer le formulaire
		pour sauvegarder les changements.',

	// L
	'label_texte' => 'Texte',

	// S
	'enlever_cet_element' => 'Enlever cet élément',

	// T
	'titre_page_configurer_elements' => 'Configurer le plugin Éléments',
);

?>
